require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.empty? ? 0 : arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|string| substring?(string, substring)}
end

def substring?(str1, str2)
  str1.include?(str2)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = []
  ("a".."z").each {|letter| letters << letter if string.split("").count(letter) > 1}
  letters.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.gsub(/[^a-zA-Z ]/, "").split(" ").sort_by {|word| word.length}.reverse.shift(2)
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  array = []
  ("a".."z").each {|letter| array << letter unless string.include?(letter)}
  array
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  results = []
  (first_yr..last_yr).each {|year| results << year if not_repeat_year?(year)}
  results
end

def not_repeat_year?(year)
  year.to_s.each_char {|i| return false if year.to_s.count(i) > 1}
  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonders = []
  songs.each {|song| wonders << song if no_repeats?(song, songs)}
  wonders.uniq
end

def no_repeats?(song_name, song_arr)
  song_arr.each_index {|idx| return false if song_arr[idx] == song_arr[idx + 1] && song_arr[idx] == song_name}
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  c_word = ""
  string.gsub(/[^a-zA-Z ]/, "").split(" ").each do |word|
    if word.include?("c")
      c_word = word if c_word == ""
      c_word = word if c_distance(word) < c_distance(c_word)
    end
  end
  c_word
end

def c_distance(word)
  word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  results = []
  start_idx = -1
  end_idx = -1
  idx_count = 0
  arr.each_with_index do |i, idx|
    if i == arr[idx + 1]
      start_idx = idx
      idx_count = idx + 1
      while i == arr[idx_count]
        end_idx = idx_count
        idx_count += 1
      end
      results << [start_idx, end_idx] unless results.flatten.include?(end_idx)
    end
  end
  results
end
